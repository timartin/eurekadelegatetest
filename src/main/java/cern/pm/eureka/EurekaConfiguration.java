package cern.pm.eureka;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.eureka.server.event.EurekaInstanceRegisteredEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

import com.netflix.appinfo.InstanceInfo;

import cern.cmw.directory.client.DirectoryClient;
import cern.cmw.directory.client.DirectoryClientBuilder;
import cern.cmw.directory.client.ServerInfo;
import cern.cmw.directory.client.exception.DirectoryException;

@SpringBootApplication
@EnableEurekaServer
public class EurekaConfiguration {

    @Autowired
    DirectoryClient directoryClient;

    @Bean
    public DirectoryClient directoryClient() {
        System.setProperty("cmw.directory.env", "TEST");
        return DirectoryClientBuilder.newInstance().build();
    }

    @EventListener
    public void handleRegisteredEvent(EurekaInstanceRegisteredEvent event) throws DirectoryException {
        if(!event.isReplication()) {
            InstanceInfo instanceInfo = event.getInstanceInfo();
            String appName = instanceInfo.getAppName();
            String hostName = instanceInfo.getHostName();
            HashSet<String> endpoints = getEndpoints(appName);

            if (InstanceInfo.InstanceStatus.UP.equals(instanceInfo.getStatus())) {
                endpoints.add(hostName);
            } else if (InstanceInfo.InstanceStatus.DOWN.equals(instanceInfo.getStatus())) {
                endpoints.remove(hostName);
            }

            directoryClient.bind(ServerInfo.createRDA3(appName, String.join(",", endpoints)));
        }
    }

    private HashSet<String> getEndpoints(String appName) throws DirectoryException {
        try {
            ServerInfo serverInfo = directoryClient.getServerInfo(appName);
            return new HashSet<>(Arrays.asList(serverInfo.getEndpoint().split(",")));
        } catch (DirectoryException exception) {
            if (exception.getMessage().contains("is not known in CMW Directory Service")) {
                return new HashSet<>();
            }
            throw exception;
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(EurekaConfiguration.class);
    }
}
